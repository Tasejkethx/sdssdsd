<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function getMessage(Request $request){
        $user = auth()->user();
        broadcast(new \App\Events\ChatStatusUpdated($request->input('message'), $user));
        return $user;
    }

}
